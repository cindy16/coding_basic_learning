﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class T3XT : MonoBehaviour
{
    [SerializeField] [TextArea] string text = "Text";
    [SerializeField] string fontFolderPath = "Assets\\T3XT\\Fonts\\Bfont", fontFileExtension = "obj";
    [SerializeField] float letterSpacing = 0.1f, wordSpacing = 0.2f, lineSpacing = 1f;

    public void BuildMesh()
    {
        //List<CombineInstance> combine = new List<CombineInstance>();
        CombineInstance[] combine = new CombineInstance[text.Length];

        Matrix4x4 matrix = Matrix4x4.identity;
        int lines = 1;
        bool lastCharWasSpecial = true;

        for(int i = 0; i < text.Length; i++)
        {
            int ascii = Convert.ToInt32(text[i]);

            switch (ascii)
            {
                case (' '):
                    matrix *= Matrix4x4.Translate(Vector3.left * (wordSpacing + (combine[i - 1].mesh != null ? combine[i - 1].mesh.bounds.size.x : 0)));
                    lastCharWasSpecial = true;
                    break;

                case (9):   // Tab
                    matrix *= Matrix4x4.Translate(Vector3.left * (wordSpacing * 4 + (combine[i - 1].mesh != null ? combine[i - 1].mesh.bounds.size.x : 0)));
                    break;

                case ('\n'):
                case ('\r'):
                    matrix = Matrix4x4.identity * Matrix4x4.Translate(Vector3.down * lineSpacing * lines);
                    lines++;
                    lastCharWasSpecial = true;
                    break;

                default:
                    if (!lastCharWasSpecial)
                        matrix *= Matrix4x4.Translate(Vector3.left * (letterSpacing + (combine[i - 1].mesh != null ? combine[i - 1].mesh.bounds.size.x : 0)));

                    string path = System.IO.Path.Combine(fontFolderPath, Convert.ToInt32(text[i]).ToString()) + "." + fontFileExtension;

                    if (System.IO.File.Exists(path))
                    {
                        Mesh cMesh = AssetDatabase.LoadAssetAtPath<Mesh>(path);
                        combine[i].mesh = cMesh;
                        combine[i].transform = matrix;
                    }
                    else
                    {
                        Debug.LogError("No mesh found for ASCII " + ascii + " (" + text[i] + ")");
                    }

                    lastCharWasSpecial = false;
                    break;
            }
        }

        GetComponent<MeshFilter>().sharedMesh = new Mesh();
        GetComponent<MeshFilter>().sharedMesh.CombineMeshes(combine);
    }
}
