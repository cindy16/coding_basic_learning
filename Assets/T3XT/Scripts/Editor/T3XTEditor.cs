﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(T3XT))]
public class T3XTEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        T3XT script = (T3XT)target;

        if (GUILayout.Button("Build Mesh"))
        {
            Undo.RecordObject(script.GetComponent<MeshFilter>(), "Build Mesh");
            script.BuildMesh();
        }
    }

    [MenuItem("GameObject/3D Object/Text - T3XT")]
    static void Create()
    {
        GameObject obj = new GameObject("Text (T3XT)");
        T3XT script = obj.AddComponent<T3XT>();
        script.BuildMesh();
        obj.GetComponent<MeshRenderer>().sharedMaterial = AssetDatabase.GetBuiltinExtraResource<Material>("Default-Material.mat");
        Selection.activeGameObject = obj;

        Undo.RegisterCreatedObjectUndo(obj, "Created Text (T3XT)");
    }
}
