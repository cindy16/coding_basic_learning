﻿using EasyInteract;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyInteractInteractorFirstPerson : EasyInteractInteractor
{
    public List<KeyCode> InteractButtons = new List<KeyCode>() { KeyCode.E, KeyCode.Return, KeyCode.KeypadEnter };

    protected override bool CheckRaycast()
    {
        return Physics.Raycast(transform.position, transform.forward, out hitInfo, interactionRange, layerMask);
    }

    protected override bool CheckInteracting()
    {
        foreach (KeyCode button in InteractButtons)
            if (Input.GetKeyDown(button))
                return true;

        return false;
    }
}
