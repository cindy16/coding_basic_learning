﻿using EasyInteract;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyInteractExampleCustomInteractablesHoverable : MonoBehaviour, EasyInteractIHoverable
{
    public void StartHover()
    {
        GetComponent<Renderer>().material.color = Color.red;
    }

    public void EndHover()
    {
        GetComponent<Renderer>().material.color = Color.blue;
    }
}
