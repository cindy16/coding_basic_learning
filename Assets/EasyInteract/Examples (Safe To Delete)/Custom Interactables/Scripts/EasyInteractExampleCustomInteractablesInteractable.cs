﻿using EasyInteract;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyInteractExampleCustomInteractablesInteractable : MonoBehaviour, EasyInteractIInteractable
{
    public virtual void Interact()
    {
        GetComponent<Renderer>().material.color = Color.green;
    }
}
