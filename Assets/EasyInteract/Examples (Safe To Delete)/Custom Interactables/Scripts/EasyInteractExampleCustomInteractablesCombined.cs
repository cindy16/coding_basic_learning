﻿using EasyInteract;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyInteractExampleCustomInteractablesCombined : MonoBehaviour, EasyInteractIHoverable, EasyInteractIInteractable
{
    public void StartHover()
    {
        GetComponent<Renderer>().material.color = Color.red;
    }

    public void EndHover()
    {
        GetComponent<Renderer>().material.color = Color.blue;
    }

    public void Interact()
    {
        GetComponent<Renderer>().material.color = Color.green;
    }
}
