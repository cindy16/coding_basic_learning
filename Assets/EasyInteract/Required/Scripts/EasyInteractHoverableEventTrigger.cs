﻿using EasyInteract;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EasyInteractHoverableEventTrigger : MonoBehaviour, EasyInteractIHoverable
{
    [SerializeField] public UnityEvent OnHoverStart, OnHoverEnd;

    public void StartHover()
    {
        OnHoverStart.Invoke();
    }

    public void EndHover()
    {
        OnHoverEnd.Invoke();
    }
}
