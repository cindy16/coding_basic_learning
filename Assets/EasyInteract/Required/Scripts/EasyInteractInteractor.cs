﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EasyInteract
{
    public class EasyInteractInteractor : MonoBehaviour
    {
        public float interactionRange = 5;
        public LayerMask layerMask;
        
        protected RaycastHit hitInfo;

        protected Transform hitThisFrame = null;
        protected Transform hitLastFrame = null;

        protected virtual bool CheckRaycast()
        {
            return Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo, interactionRange, layerMask);
        }

        protected virtual bool CheckInteracting()
        {
            return Input.GetMouseButtonDown(0);
        }

        private void Update()
        {
            if (CheckRaycast())
                hitThisFrame = hitInfo.transform;
            else
                hitThisFrame = null;

            if (hitThisFrame != hitLastFrame)
            {
                EasyInteractIHoverable hoverable = null;

                if(hitThisFrame != null && (hoverable = hitThisFrame.GetComponent<EasyInteractIHoverable>()) != null)
                    hoverable.StartHover();

                if (hitLastFrame != null && (hoverable = hitLastFrame.GetComponent<EasyInteractIHoverable>()) != null)
                    hoverable.EndHover();
            }

            if(hitThisFrame != null)
            {
                bool interacting = CheckInteracting();

                if (interacting)
                {
                    EasyInteractIInteractable interactable = null;

                    if ((interactable = hitThisFrame.GetComponent<EasyInteractIInteractable>()) != null)
                        interactable.Interact();
                }
            }

            hitLastFrame = hitThisFrame;
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawLine(transform.position, transform.position + transform.forward * interactionRange);
            Gizmos.DrawSphere(transform.position, 0.025f);
        }
    }
}
