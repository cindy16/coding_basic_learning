﻿using EasyInteract;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EasyInteractInteractableEventTrigger : MonoBehaviour, EasyInteractIInteractable
{
    [SerializeField] public UnityEvent OnInteract;
    public void Interact()
    {
        OnInteract.Invoke();
    }
}
