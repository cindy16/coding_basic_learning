﻿namespace EasyInteract
{
    public interface EasyInteractIHoverable
    {
        void StartHover();

        void EndHover();
    }

    public interface EasyInteractIInteractable
    {
        void Interact();
    }
}
