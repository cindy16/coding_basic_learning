﻿using EasyInteract;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyInteractInteractorOculus : EasyInteractInteractor
{
    public List<OVRInput.RawButton> InteractButtons = new List<OVRInput.RawButton>() { OVRInput.RawButton.A, OVRInput.RawButton.RHandTrigger };

    protected override bool CheckRaycast()
    {
#if UNITY_EDITOR
        return base.CheckRaycast();
#else
        return Physics.Raycast(transform.position, transform.forward, out hitInfo, interactionRange, layerMask);
#endif
    }

    protected override bool CheckInteracting()
    {
#if UNITY_EDITOR
        return base.CheckInteracting();
#else
        foreach (OVRInput.RawButton button in InteractButtons)
            if (OVRInput.GetDown(button))
                return true;

        return false;
#endif
    }
}
