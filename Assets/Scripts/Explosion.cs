﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    [SerializeField] float range;
    [SerializeField] float force;
    // Start is called before the first frame update
    public void Explode()
    {
        Collider[] colliders;
        colliders = Physics.OverlapSphere(transform.position, range);
        foreach (Collider object_collider in colliders)
        {
            Rigidbody rigidbody = object_collider.GetComponent<Rigidbody>();
            if (rigidbody !=null)
            {
                rigidbody.AddExplosionForce(force, transform.position, range);
            }
        
        }
            // goes tho all each object, similar to loop function 


    }


    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
