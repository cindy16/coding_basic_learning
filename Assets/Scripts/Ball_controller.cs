﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_controller : MonoBehaviour
{
    Rigidbody rigidbody;
    [SerializeField] float force;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody = gameObject.GetComponent<Rigidbody>();
        rigidbody.AddForce(transform.forward*force);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
