﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving_objects : MonoBehaviour
{
    [SerializeField] float speed=20;
    public void move_object(GameObject object_to_move)
    {
        Vector3 input = new Vector3();
        // making new vector3 to modify the value
        input.x = Input.GetAxis("Horizontal");
        input.y = Input.GetAxis("Vertical");
        input.z = Input.GetAxis("Depth");
        object_to_move.transform.Translate(input * speed * Time.deltaTime);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    
    }
    
}
