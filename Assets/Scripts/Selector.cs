﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : MonoBehaviour
{
    GameObject SelectedObject;
    [SerializeField] Color selectColor = Color.white ;
    [SerializeField] Moving_objects mover;
    Color original_color;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Select"))
        {
            if(SelectedObject != null)
            {
                SelectedObject.GetComponent<Renderer>().material.color = original_color;
            }
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                SelectedObject = hitInfo.transform.gameObject;
                original_color = SelectedObject.GetComponent<Renderer>().material.color;
                SelectedObject.GetComponent<Renderer>().material.color = selectColor;
                Debug.Log(SelectedObject.name);
            }
            else
            {
                SelectedObject = null;
            }
            

        }
        if(SelectedObject != null)
        {
            mover.move_object(SelectedObject);  
        }
    }
}
