﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickOn : MonoBehaviour
{
    public Material blue;
    public Material red;

    public MeshRenderer myMaterial;
    void Start()
    {
        myMaterial = GetComponent<MeshRenderer>();
    }

    public void ClickMe()
    {
        myMaterial.material = blue;
    }
    void Update()
    {
        
    }
}
