﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_characters : MonoBehaviour
{
    CharacterController characterController;
    [SerializeField] float speed = 20;
    [SerializeField] float mouse_sensitivity = 15;

    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up, Input.GetAxis("Mouse Horizontal")*mouse_sensitivity*Time.deltaTime);
        Vector3 input = new Vector3();
        input += transform.forward * Input.GetAxis("Vertical");
        input += transform.right * Input.GetAxis("Horizontal");
        input += transform.up * Input.GetAxis("Depth");
            //get the vector forward from the object, depend on the direction facing.
        //input.x = Input.GetAxis("Horizontal");
        //input.y = Input.GetAxis("Vertical");
        //input.z = Input.GetAxis("Depth");

        characterController.Move(input * speed * Time.deltaTime);
        
    }

}
