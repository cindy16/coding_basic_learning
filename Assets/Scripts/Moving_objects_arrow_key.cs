﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving_objects_arrow_key : MonoBehaviour
{
    public float moveSpeed; 
    // Start is called before the first frame update
    void Start()
    {
        moveSpeed = 10f;
    }

    // Update is called once per frame
    void Update()
    {
        //print("this is a single kitchen block");
        //transform.Translate(0f,1f * Time.deltaTime, 0f);
        //print(Input.GetAxis("Vertical"));
        transform.Translate(moveSpeed*-Input.GetAxis("Horizontal")*Time.deltaTime, 0f,moveSpeed*Input.GetAxis("Vertical") * Time.deltaTime);
    }
}
