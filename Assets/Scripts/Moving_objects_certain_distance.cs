﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving_objects_certain_distance : MonoBehaviour
{
    public float speed = 5f;
    public float momentum = 0;
    public float damping = 5;
    Vector3 destination;
    void Start()
    {
        
    }

    void Update()
    {
        float momentumSpeed = 5f;
        float damping = 5;
        bool arrowKeysUp = true;
        float momentum = 0;
        if (Input.GetAxis("Horizontal") != 0f || Input.GetAxis("Vertical") != 0f && arrowKeysUp == true)
        {
            momentum = momentumSpeed;

            arrowKeysUp = false;
        }

        else
            arrowKeysUp = true;
        transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * momentum * Time.deltaTime);

        transform.Translate(Vector3.up * Input.GetAxis("Vertical") * momentum * Time.deltaTime);

        if (momentum > 0f)
            momentum -= (damping * Time.deltaTime);
    }
}
