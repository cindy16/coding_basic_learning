﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VR_movement : MonoBehaviour
{ [SerializeField] OVRInput.RawButton button;
    [SerializeField] GameObject destination;
    [SerializeField] GameObject rig;
    bool buttonHeld = false;
    float range = 3;
    [SerializeField] LayerMask layerMask;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(button))
        {
            destination.SetActive(true);
            buttonHeld = true;
        }
        else if (OVRInput.GetUp(button))
        {
            destination.SetActive(false);
            buttonHeld = false;
            rig.transform.position = destination.transform.position;
        }
        if(buttonHeld == true)
        {
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hitInfo;

            if(Physics.Raycast(ray, out hitInfo, range, layerMask))
            {
                destination.transform.position = hitInfo.point;
            }
        }
    }
}
